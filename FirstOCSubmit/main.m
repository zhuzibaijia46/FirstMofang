//
//  main.m
//  FirstOCSubmit
//
//  Created by 诸子百家 on 2018/12/9.
//  Copyright © 2018 zzbj. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
