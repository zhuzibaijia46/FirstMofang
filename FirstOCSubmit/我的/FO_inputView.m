//
//  FO_inputView.m
//  FirstOCSubmit
//
//  Created by 诸子百家 on 2018/12/9.
//  Copyright © 2018 zzbj. All rights reserved.
//

#import "FO_inputView.h"

@implementation FO_inputView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        //background
        UIView *userV = [UIView new];
        [self addSubview:userV];
        userV.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
        userV.layer.borderColor = GrayColor(216).CGColor;
        userV.layer.borderWidth = 1;
        
        //phone view
        UIImageView *phoneV = [UIImageView new];
        _iconIV = phoneV;
        [userV addSubview:phoneV];
        phoneV.sd_layout
        .widthIs(20)
        .heightIs(25)
        .leftSpaceToView(userV, 20)
        .centerYEqualToView(userV);
        
        //center view
        UIView *centerV = [UIView new];
        centerV.backgroundColor = GrayColor(216);
        [userV addSubview:centerV];
        centerV.sd_layout
        .heightIs(20)
        .widthIs(1)
        .centerYEqualToView(userV)
        .leftSpaceToView(phoneV, 23);
        
        //input field
        UITextField *inputField = [UITextField new];
        _textTF = inputField;
        inputField.font = kFONT(13);
        [userV addSubview:inputField];
        inputField.sd_layout
        .leftSpaceToView(centerV, 10)
        .heightRatioToView(userV, 0.9)
        .centerYEqualToView(userV)
        .rightSpaceToView(userV, 10);
        
    }
    return self;
}

-(void)setIconStr:(NSString *)iconStr{
    _iconIV.image = [UIImage imageNamed:iconStr];
}

@end
