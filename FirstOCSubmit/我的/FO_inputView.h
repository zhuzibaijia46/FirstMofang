//
//  FO_inputView.h
//  FirstOCSubmit
//
//  Created by 诸子百家 on 2018/12/9.
//  Copyright © 2018 zzbj. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FO_inputView : UIView

@property(nonatomic,copy) NSString * iconStr;
@property(nonatomic,weak) UIImageView * iconIV;
@property(nonatomic,weak) UITextField * textTF;

@end

NS_ASSUME_NONNULL_END
