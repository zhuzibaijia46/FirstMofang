//
//  MineController.m
//  FirstOCSubmit
//
//  Created by 诸子百家 on 2018/12/9.
//  Copyright © 2018 zzbj. All rights reserved.
//

#import "MineController.h"
#import "FO_inputView.h"
@interface MineController ()

@property(nonatomic,weak) UIButton *codeBtn;
@property(nonatomic,weak) FO_inputView *phoneView1;
@property(nonatomic,weak) FO_inputView *phoneView2;
@property(nonatomic,weak) FO_inputView *pwdView1;
@property(nonatomic,weak) FO_inputView *pwdView2;
@property(nonatomic,weak) FO_inputView *codeView;
@property(nonatomic,weak) UIView *midV2;
@property(nonatomic,weak) UIView * selectV;
@property(nonatomic,weak) UIButton *leftBtn;
@property(nonatomic,weak) UIButton *rightBtn;
@property(nonatomic,strong) UIButton *loginBtn;

@end

@implementation MineController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor redColor];
    
    UIImageView *baseIV = [UIImageView new];
    baseIV.image = [UIImage imageNamed:@"beijing"];
    [self.view addSubview:baseIV];
    baseIV.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
    
    self.navigationItem.title = @"登陆";
    [self setupViews];
    
}

-(void)setupViews {
    //top left login
    UIButton *lgBtn = [UIButton new];
    _leftBtn = lgBtn;
    [lgBtn setTitle:@"登陆" forState:UIControlStateNormal];
    [lgBtn setTitleColor:RGBColor(255, 90, 55) forState:UIControlStateNormal];
    [lgBtn setTitleColor:GrayColor(175) forState:UIControlStateSelected];
    lgBtn.titleLabel.font = kFONT(15);
    [self.view addSubview:lgBtn];
    lgBtn.sd_layout
    .heightIs(30)
    .widthIs(97)
    .rightSpaceToView(self.view, kScreenWidth/2)
    .topSpaceToView(self.view, 43+NavHeight);
    [lgBtn addTarget:self action:@selector(leftClick) forControlEvents:UIControlEventTouchUpInside];
    
    //top right register
    UIButton *rgBtn = [UIButton new];
    _rightBtn = rgBtn;
    [rgBtn setTitle:@"注册" forState:UIControlStateNormal];
    [rgBtn setTitleColor:RGBColor(255, 90, 55) forState:UIControlStateNormal];
    [rgBtn setTitleColor:GrayColor(175) forState:UIControlStateSelected];
    rgBtn.titleLabel.font = kFONT(15);
    [self.view addSubview:rgBtn];
    rgBtn.sd_layout
    .heightIs(30)
    .widthIs(97)
    .leftSpaceToView(self.view, kScreenWidth/2)
    .topSpaceToView(self.view, 43+NavHeight);
    [rgBtn addTarget:self action:@selector(rightClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *lineBg = [UIView new];
    lineBg.backgroundColor = GrayColor(208);
    [self.view addSubview:lineBg];
    lineBg.sd_layout
    .widthIs(2*97)
    .heightIs(3)
    .leftEqualToView(lgBtn)
    .topSpaceToView(lgBtn, 0);
    
    UIView *animaLine = [UIView new];
    _selectV = animaLine;
    animaLine.backgroundColor = RGBColor(255, 90, 55);
    [self.view addSubview:animaLine];
    animaLine.sd_layout
    .widthIs(97)
    .heightIs(3)
    .leftEqualToView(lgBtn)
    .topSpaceToView(lgBtn, 0);
    
    UIImageView *boImg = [UIImageView new];
    boImg.image = [UIImage imageNamed:@"background"];
    [self.view addSubview:boImg];
    boImg.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .bottomEqualToView(self.view)
    .heightIs(116);
    
    UIView *midV1 = [UIView new];
    [self.view addSubview:midV1];
    midV1.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .topSpaceToView(animaLine, 0)
    .bottomSpaceToView(boImg, 0);
    
    FO_inputView *phoneV = [FO_inputView new];
    _phoneView1 = phoneV;
    phoneV.textTF.placeholder = @"请输入手机号码";
    phoneV.iconStr = @"icon_registered_mobile";
    [midV1 addSubview:phoneV];
    phoneV.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV1)
    .topSpaceToView(midV1, 56);

    FO_inputView *psdV = [FO_inputView new];
    _pwdView1 = psdV;
    psdV.textTF.placeholder = @"请输入密码";
    psdV.iconStr = @"icon_registered_password";
    [midV1 addSubview:psdV];
    psdV.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV1)
    .topSpaceToView(phoneV, 30);
    
    UIButton *loginBtn = [UIButton new];
    [loginBtn addTarget:self action:@selector(loginBtnClick) forControlEvents:UIControlEventTouchUpInside];
    loginBtn.titleLabel.font = kFONT(17);
    [loginBtn setTitle:@"登陆" forState:UIControlStateNormal];
    [midV1 addSubview:loginBtn];
    loginBtn.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV1)
    .topSpaceToView(psdV, 45);
    [loginBtn setBackgroundImage:[UIImage imageNamed:@"icon_registered_button_backgriund"] forState:UIControlStateNormal];
    self.loginBtn = loginBtn;
    
    UIButton *forgetBtn = [UIButton new];
    [forgetBtn addTarget:self action:@selector(forgetBtnClick) forControlEvents:UIControlEventTouchUpInside];
    forgetBtn.titleLabel.font = kFONT(13);
    [forgetBtn setTitleColor:RGBColor(245, 123, 82) forState:UIControlStateNormal];
    [forgetBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [midV1 addSubview:forgetBtn];
    forgetBtn.sd_layout
    .widthIs(100)
    .heightIs(44)
    .rightEqualToView(loginBtn)
    .topSpaceToView(loginBtn, 45);
    forgetBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, -20);
    
    UIView *midV2 = [UIView new];
    midV2.hidden = YES;
    _midV2 = midV2;
    [self.view addSubview:midV2];
    midV2.sd_layout
    .leftEqualToView(self.view)
    .rightEqualToView(self.view)
    .topSpaceToView(animaLine, 0)
    .bottomSpaceToView(boImg, 0);
    
    FO_inputView *phoneV2 = [FO_inputView new];
    _phoneView2 = phoneV2;
    phoneV2.textTF.placeholder = @"请输入手机号码";
    phoneV2.iconStr = @"icon_registered_mobile";
    [midV2 addSubview:phoneV2];
    phoneV2.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV2)
    .topSpaceToView(midV2, 56);
    
    FO_inputView *psdV2 = [FO_inputView new];
    _pwdView2 = psdV2;
    psdV2.textTF.placeholder = @"请输入密码";
    psdV2.iconStr = @"icon_registered_password";
    [midV2 addSubview:psdV2];
    psdV2.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV2)
    .topSpaceToView(phoneV2, 30);
    
    FO_inputView *codeView = [FO_inputView new];
    _codeView = codeView;
    codeView.textTF.placeholder = @"请输入验证码";
    codeView.iconStr = @"icon_registered_verification";
    [midV2 addSubview:codeView];
    codeView.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV2)
    .topSpaceToView(psdV2, 30);
    
    UIButton *codeBtn = [UIButton new];
    [codeBtn addTarget:self action:@selector(codeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    codeBtn.titleLabel.font = kFONT(13);
    [codeBtn setTitleColor:RGBColor(245, 123, 82) forState:UIControlStateNormal];
    [codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [codeView addSubview:codeBtn];
    codeBtn.sd_layout
    .widthIs(76)
    .heightIs(26)
    .centerYEqualToView(codeView)
    .rightSpaceToView(codeView, 10);
    _codeBtn = codeBtn;
    
    UIButton *regBtn = [UIButton new];
    [regBtn addTarget:self action:@selector(regBtnClick) forControlEvents:UIControlEventTouchUpInside];
    regBtn.titleLabel.font = kFONT(17);
    [regBtn setTitle:@"注册" forState:UIControlStateNormal];
    [midV2 addSubview:regBtn];
    regBtn.sd_layout
    .widthIs(271)
    .heightIs(44)
    .centerXEqualToView(midV2)
    .topSpaceToView(codeView, 45);
    [regBtn setBackgroundImage:[UIImage imageNamed:@"icon_registered_button_backgriund"] forState:UIControlStateNormal];
    
    
}

-(void)leftClick{
    NSLog(@"leftClick");
    self.leftBtn.enabled = NO;
    self.rightBtn.enabled = NO;
    [_leftBtn setTitleColor:RGBColor(255, 90, 55) forState:UIControlStateNormal];
    [_rightBtn setTitleColor:GrayColor(175) forState:UIControlStateNormal];
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame =  self->_selectV.frame;
        frame.origin.x = self->_leftBtn.mj_x;
        self->_selectV.frame = frame;
        self.midV2.alpha = 0;
    } completion:^(BOOL finished) {
        self.leftBtn.enabled = YES;
        self.rightBtn.enabled = YES;
        self.midV2.hidden = YES;
        self.navigationItem.title = @"登陆";
        self.loginBtn.hidden = NO;
    }];
    
}

-(void)rightClick{
    NSLog(@"rightClick");
    self.leftBtn.enabled = NO;
    self.rightBtn.enabled = NO;
    [_rightBtn setTitleColor:RGBColor(255, 90, 55) forState:UIControlStateNormal];
    [_leftBtn setTitleColor:GrayColor(175) forState:UIControlStateNormal];
    self.midV2.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame =  self->_selectV.frame;
        frame.origin.x = self->_rightBtn.mj_x;
        self->_selectV.frame = frame;
        self.midV2.alpha = 1;
    } completion:^(BOOL finished) {
        self.leftBtn.enabled = YES;
        self.rightBtn.enabled = YES;
        self.midV2.hidden = NO;
        self.loginBtn.hidden = YES;
        self.navigationItem.title = @"注册";
    }];
}

-(void)loginBtnClick{
    NSLog(@"loginBtnClick");
}

-(void)forgetBtnClick{
    NSLog(@"forgetBtnClick");
}

-(void)codeBtnClick{
    NSLog(@"codeBtnClick");
}

-(void)regBtnClick{
    NSLog(@"regBtnClick");
}
@end
